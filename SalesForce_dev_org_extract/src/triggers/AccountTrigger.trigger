trigger AccountTrigger on Account (after insert,after update) {

    //final String URL = 'http://www.hidegver.nhely.hu/sf/ApaPhp2/account.php?acc=';
    final String URL  = URLS__c.getInstance().UrlAccountTrigger__c;
    
    List<Account> aList = trigger.new;
    String msg ='';

    
    Integer commaCount=0;
    for (Account a : aList){
        String addressAcc = a.BillingCity.replace(',', '|');
        msg += a.Id + ',' + a.Name +',' + a.Phone  +',' + addressAcc;
        System.debug('*************billing: '+ addressAcc);
        commaCount++;
        if (commaCount>1){
            msg +='\n';
        }
    }
    HttpContactInsert.sendContents(msg, URL);
    
}
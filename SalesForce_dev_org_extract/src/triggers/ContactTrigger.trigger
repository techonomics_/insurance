trigger ContactTrigger on Contact (after insert) {

    //final String URL = 'http://www.hidegver.nhely.hu/sf/ApaPhp2/index.php?age=';
    final String URL = URLS__c.getInstance().UrlContactTrigger__c;
    
    List<Contact> cList = trigger.new;
    String msg ='';
    
  
    Integer i=0;
    for (Contact c2 : cList){
        msg += c2.Email + ',' + c2.Id +',' + c2.AccountId + ','+c2.FirstName + ','+c2.LastName;
        i++;
        if (i>1){
            msg +='\n';
        }
    }
    
    HttpContactInsert.sendContents(msg, URL);
}
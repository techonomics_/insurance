trigger VehicleTrigger on Vehicle__c (after insert) {

    if(trigger.isAfter){
        if(trigger.isInsert){
            VehicleToProductHandler.handleAfterInsert(trigger.new);
        }
    }
    
    
    
}
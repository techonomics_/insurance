trigger CaseFromWebTrigger on Case (after insert) {

    List<Case> caseList = trigger.new;
    TaskFromCase.createTasks(caseList);

}
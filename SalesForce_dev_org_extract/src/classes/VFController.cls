/**
 * Created by Csaba on 2020. 01. 24..
 */

public class VFController {
    private final Opportunity o;
    public VFController(ApexPages.StandardController stdController) {
        this.o = (Opportunity)stdController.getRecord();
    }

    // Code we will invoke on page load.
    public PageReference autoRun() {

        String theId = ApexPages.currentPage().getParameters().get('id');

        if (theId == null) {
            // Display the Visualforce page's content if no Id is passed over
            return null;
        }

        for (Opportunity o:[SELECT ID,closeDate,AccountId,Name FROM Opportunity]) {
            // Do all the dirty work we need the code to do
        }

        // Redirect the user back to the original page
        PageReference pageRef = new PageReference('/' + theId);
        pageRef.setRedirect(true);
        return pageRef;

    }
}
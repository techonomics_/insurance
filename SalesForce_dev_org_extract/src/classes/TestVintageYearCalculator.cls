@isTest(SeeallData = true)
public class TestVintageYearCalculator {

    @isTest
    public static void Are_Custom_Settings_Values_Accessible(){
        
        VehicleYear__c vehYear = VehicleYear__c.getInstance();
        Decimal YEAR_BASE = vehYear.BaseYear__c;
        Decimal YEAR_PIVOT= vehYear.PivotYear__c;
        Decimal YEAR_PRESENT= vehYear.PresentYear__c;
        
        PriceFactor__c pFactor = PriceFactor__c.getInstance();
        Double FACTOR_YEAR_BASE = pFactor.FactorBaseYear__c;
        Double FACTOR_YEAR_PIVOT= pFactor.FactorPivotYear__c;
        Double FACTOR_YEAR_PRESENT= pFactor.FactorPresentYear__c;
        
        System.assertNotEquals(null, YEAR_BASE);
        System.assertNotEquals(null, YEAR_PIVOT);
        System.assertNotEquals(null, YEAR_PRESENT);
        System.assertNotEquals(null, FACTOR_YEAR_BASE);
        System.assertNotEquals(null, FACTOR_YEAR_PIVOT);
        System.assertNotEquals(null, FACTOR_YEAR_PRESENT);        
        
        
    } 
  
    
    @isTest
    public static void Alter_Year_Custom_Settings_Value(){
        
     //   VehicleYear__c vintageCustom = [SELECT Id,Name,BaseYear__c,PivotYear__c,PresentYear__c FROM VehicleYear__c LIMIT 1];
     //   PriceFactor__c priceFactorCustom = [SELECT Id,Name,FactorBaseYear__c,FactorPivotYear__c,FactorPresentYear__c FROM PriceFactor__c LIMIT 1];
        
        VehicleYear__c vintageCustom = new VehicleYear__c();
        PriceFactor__c priceFactorCustom = new PriceFactor__c(); 
        
        vintageCustom.BaseYear__c = 1979;
        vintageCustom.PivotYear__c= 2013;
        vintageCustom.PresentYear__c=2020;
        priceFactorCustom.FactorBaseYear__c = 3.5;
        priceFactorCustom.FactorPivotYear__c= 0.6;
        priceFactorCustom.FactorPresentYear__c=1.2;
        
        insert vintageCustom;
        insert priceFactorCustom;
        VintageCalculator.calculateFactorFromVintage();
        
        System.debug('********QUERY*****' + vintageCustom);
        System.debug('********QUERY*****' + priceFactorCustom);        
        
        List<VehicleYear__c> tryout = [SELECT BaseYear__c FROM VehicleYear__c];
        System.debug('******MI VAN A VEH LISTABAN ' + tryout);
        
  		Decimal factorAltered = VintageCalculator.getPricefactorFromCache((Integer)tryout.get(0).BaseYear__c);
        System.assertEquals(priceFactorCustom.FactorBaseYear__c, factorAltered);
        
        
    //    VehicleYear__c test = new VehicleYear__c();
    //    PriceFactor__c dummy2 = new PriceFactor__c();
        
        
    }
    
    
    @isTest
    public static void Are_Boundary_Factors_Correct(){
        
        VehicleYear__c vehYear = VehicleYear__c.getInstance();
        Decimal YEAR_BASE = vehYear.BaseYear__c;
        Decimal YEAR_PIVOT= vehYear.PivotYear__c;
        Decimal YEAR_PRESENT= vehYear.PresentYear__c;
        
        PriceFactor__c pFactor = PriceFactor__c.getInstance();
        Double FACTOR_YEAR_BASE = pFactor.FactorBaseYear__c;
        Double FACTOR_YEAR_PIVOT= pFactor.FactorPivotYear__c;
        Double FACTOR_YEAR_PRESENT= pFactor.FactorPresentYear__c;
        
        Decimal lowerBoundary =  VintageCalculator.getPricefactorFromCache(1980);
        Decimal upperBoundary =  VintageCalculator.getPricefactorFromCache(2020);        
        
        
        System.assertEquals(FACTOR_YEAR_BASE, lowerBoundary);
        System.assertEquals(FACTOR_YEAR_PRESENT, upperBoundary);        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
}
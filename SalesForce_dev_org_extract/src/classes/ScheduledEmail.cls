global class ScheduledEmail implements Schedulable{
static final String BOSS_EMAIL_ADDRESS = 'bruteforcebh10@gmail.com';
    
global void execute(SchedulableContext SC) {
    
        String body='';
 List<Opportunity> expiringOpportunitiesList = 
     [SELECT Id, name, stageName,closeDate 
      FROM Opportunity 
      WHERE stageName !='Closed Won' 
      AND closeDate=:System.today()]; 
        for(Opportunity o: expiringOpportunitiesList){
           body += o.Name +' - '+o.stageName+' - '+o.closeDate+';\n';
            
        }
        ScheduledEmail.sendMail(BOSS_EMAIL_ADDRESS, 'Expired!',body);
    }
      public static void sendMail(String address, String subject, String body) {

    
 //       expiringOpportunitiesList = 
// EmailTemplate et = [SELECT id FROM EmailTemplate WHERE developerName = 'Expired Opportunities'];
 Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {address};
        mail.setToAddresses(toAddresses);
        mail.setSubject(subject);
        mail.setPlainTextBody(body);
        // Pass this email message to the built-in sendEmail method 
        // of the Messaging class
        Messaging.SendEmailResult[] results = Messaging.sendEmail(
                                 new Messaging.SingleEmailMessage[] { mail });
        
        // Call a helper method to inspect the returned results
        inspectResults(results);    
  
}
    // Helper method
    private static Boolean inspectResults(Messaging.SendEmailResult[] results) {
        Boolean sendResult = true;
        
        // sendEmail returns an array of result objects.
        // Iterate through the list to inspect results. 
        // In this class, the methods send only one email, 
        // so we should have only one result.
        for (Messaging.SendEmailResult res : results) {
            if (res.isSuccess()) {
                System.debug('Email sent successfully');
            }
            else {
                sendResult = false;
                System.debug('The following errors occurred: ' + res.getErrors());                 
            }
        }
        
        return sendResult;
    }

}
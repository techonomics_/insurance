public with sharing class PriceBookEditorController {



    public String getString() {
        return s;
    }

    public void setString(String s) {
        this.s = s;
    }




    public List<PricebookEntry> getEntryList() {

        List<PricebookEntry> pbeList = [
                SELECT Id, Product2.Name,UnitPrice,Pricebook2.name,Product_Descr__c,Product_Cat__c
                FROM PricebookEntry
                ORDER BY Product2.Name
        ];


        for (PricebookEntry p : pbeList) {

            p.Product_Cat__c = 'Flotta CASCO ';
            p.Product_Descr__c = p.Product2.Name.remove('Flotta CASCO ');

        }


        return pbeList;
    }


    public List<SelectOption> getItems() {


        List<Brand__c> brandNamesList = [SELECT Name FROM Brand__c ORDER BY Name];
        List<SelectOption> options = new List<SelectOption>();
        for (Brand__c b : brandNamesList) {
            options.add(new SelectOption(b.Name, b.Name));
        }
        return options;
    }
    public String getCountries() {
        //If multiselect is false, countries must be of type String
        return countries;
    }
    public void setCountries(String countries) {
        //If multiselect is false, countries must be of type String
        this.countries = countries;
    }
    public PageReference test() {
        return null;
    }


    public PageReference error() {

        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, 'IN PROGRESS'));
        return null;

    }
}
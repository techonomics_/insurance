public class GenerateQuoteInPdf {
    
    @Future(callout=true)
    public static void generatePdf(Id currentQuoteId){

   String QuoteID = currentQuoteId+'';

String templateID = '0EH5J000000gZLO';    
String quoteUrl = '/quote/quoteTemplateDataViewer.apexp?id=';
 
quoteUrl +=QuoteID;
quoteUrl +='&headerHeight=190&footerHeight=188&summlid=';
quoteUrl +=templateID ; 
quoteUrl +='#toolbar=1&navpanes=0&zoom=90';

PageReference pg = new PageReference(quoteUrl) ; 
QuoteDocument quotedoc = new QuoteDocument(); 
Blob content = pg.getContentAsPDF() ;

quotedoc.Document = content;
System.debug('name?: '+content);

quotedoc.QuoteId = QuoteID ;
System.debug('quoteId?: '+QuoteID);

insert quotedoc; 
        GenerateQuoteInPdf.sendPdfQuoteToContactInEmail(content, QuoteID );
}
  
public static void sendPdfQuoteToContactInEmail(Blob content, ID QuoteID){
      Quote quote = [SELECT id,Name,Contact.name,email 
                     FROM Quote 
                     WHERE id = :QuoteID 
                     LIMIT 1]; 
      string strResponse = '';
              system.debug('before quote not null');
     system.debug('quote'+quote);
            if(quote!=null) {   
              system.debug('in quote not null');
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
                   // String [] listEmailAddress= New string[]{'surdilovics@gmail.com'};   
                    List<String> listEmailAddress = new List<String>();
                    String strEmailBody='';
                System.debug('quote email: '+quote.email);
                	listEmailAddress.add(quote.email);
              //  listEmailAddress.add('bruteforcebh10@gmail.com');
                    attachment.setFileName(quote.Name+'Quotation.pdf');
                       
                System.debug('content:'+content);
                    attachment.setBody(content);    
                System.debug(content);
                
                    strResponse = quote.email;
                    if(quote.Contact.name!=null){
                        strEmailBody = 'Hi '+ quote.Contact.name+',';
                    }else{
                        strEmailBody = 'Hi,';
                    }
                    
                    strEmailBody = strEmailBody + '\n' + '\n'+' Below is the quotation suggested by Team regarding to Quote : '+quote.name+ '\n' + '\n';
                    strEmailBody = +'\n'+'\n'+strEmailBody +'\n' + 'Thank you.';
                    mail.setSubject(quote.Name+' Quotation');
                     System.debug('quote.Name: '+quote.Name);

                  
                   System.debug('LstEmailAddrses: '+listEmailAddress);
                   mail.setToAddresses(listEmailAddress);
	                              
                    mail.setPlainTextBody(strEmailBody);
                    mail.setFileAttachments(new Messaging.EmailFileAttachment[] {attachment});
                try{
                    Messaging.SendEmailResult [] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});}
                catch(Exception ex){
                        String error = ex.getMessage();
                  
                    System.debug('*'+ex);
                }
          
              
                  Boolean sendResult = true;
       
            }
    
        }
    
      public static void opportunityStageChangedToAccepted(List<Quote> triggerNew ){
          List<Quote> quoteList = [SELECT Id, Name, Opportunity.Id, WholeQuantity__c FROM Quote WHERE Id IN:triggerNew AND Status=:'Accepted'];
          Integer numOfCars;
          
          for(Quote q : quoteList){
             numOfCars = (Integer) q.WholeQuantity__c;
          }
          System.debug('*numofcars: '+numOfcars);
          if(quoteList!=null){
 		  List<Contract>contractList=new List<Contract>();
       	  Set<Id> opportunityIdSet = getopportunityIdSet(quoteList);
      	  List<Opportunity> opportunityList = 
            [SELECT Id, Account.Name,Account.Id, Name, Contractual_period__c, Contract_Ends__c,
             Contract_start__c,CreatedById, Pricebook2Id
             FROM Opportunity
             WHERE id IN:opportunityIdSet AND StageName=:'Proposal/Price Quote'];
     
          for(Opportunity currentOpportunity:opportunityList){
     String timeStamp =  String.valueOf(datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss'));
          Contract contract = new Contract();
              
            contract.Name= currentOpportunity.Account.Name+'-' + timeStamp;
            contract.StartDate = currentOpportunity.Contract_start__c;
             contract.ContractTerm =(calculateMonth(currentOpportunity.Contractual_period__c));
          
              contract.PriceBook2Id =currentOpportunity.Pricebook2Id;
            
            contract.Status = 'Draft';
            contract.AccountId = currentOpportunity.Account.Id;
              contract.Description = descriptionGenerator(numOfCars);
          

    
       
   	      contractList.add(contract);  
   	  
     
          }
      try{
            
            upsert contractList;}
        catch(Exception ex){ 
           System.debug(ex.getMessage());
            
    }
          }
          }        
    public static Set<Id> getopportunityIdSet(List<Quote> quoteList){
        Set<Id> opportunityIdSet = new Set<Id>();
          for(Quote q : quoteList){
              opportunityIdSet.add(q.OpportunityId);
          }
       return opportunityIdSet; 
        
    }
    
	 public static Integer calculateMonth(String period){
        Switch on period {
        when '3 MONTHS'{
           return 3;
        }
        when '6 MONTHS'{
            return 6;
        }
        when '12 MONTHS'{
           return 12;
        }
        when else{
           return 12;
        }
       }
    }
public static String descriptionGenerator(Integer numberOfVehicles){
String carProperties ='';
String claimAdjuster = 'estimated by Vakvarju Bt. :\n';
List<String> cars = new List<String>();
cars.add(claimAdjuster);
for (Integer i=0;i<numberOfVehicles;i++){
carProperties += 'Plate number: ' + generateLicencePlate() + ' Color: ' + generateColor()+ ' Condition: ' +generateConditon()+ ' \n';


}
    System.debug('*** car properites: ');
return claimAdjuster + carProperties;
}
public static String generateColor(){
List<String> colorList = new List<String>{'Red','Blue','Green','White','Yellow'};
Integer lastIndex = colorList.size() - 1;
Integer randomIndex = Integer.valueOf((Math.random() * lastIndex));

return colorList[randomIndex];
}
public static String generateConditon(){
List<String> conditionList = new List<String>{'PERFECT','GOOD','AVERAGE','POOR'};
Integer lastIndex = conditionList.size() - 1;
Integer randomIndex = Integer.valueOf((Math.random() * lastIndex));

return  conditionList[randomIndex];
}
public static String generateLicencePlate(){
String plate ='';
for(Integer i=0;i<3;i++){
List<String> letterList = new List<String>{'A','B','C','D','F','Y','Z','R'};
Integer lastIndex = letterList.size() - 1;
Integer randomIndex = Integer.valueOf((Math.random() * lastIndex));
String randomLetter = letterList[randomIndex];
plate+=randomLetter;}
Integer randomPlateNumber = Integer.valueOf((Math.random() * 1000));
plate+='-'+randomPlateNumber;
return plate;
}

 }
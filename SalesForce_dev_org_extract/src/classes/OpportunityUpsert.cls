public class OpportunityUpsert {

    private static Integer startline = 1;
    
    
    public static String parseDataFromHttpConn(String httpResp){
        
        String result = '';
    	List<Opportunity> oppList = new List<Opportunity>();
        String[] httpResponseSplit = httpResp.split('\\n');
        
        
        
        for(Integer i=startline; i<httpResponseSplit.size(); ++i){
            if(!String.isEmpty(httpResponseSplit[i])){
                String[] lineSplit = httpResponseSplit[i].split(',');
                for(Integer j = 0; j<lineSplit.size(); ++j){
                    lineSplit[j] = lineSplit[j].replaceAll('"', '');
                }
             	Opportunity oo = new Opportunity();
                oo.AccountId = Id.valueOf(lineSplit[0]);  
                result += oo.AccountId + ',';
                Date todaysDate = Date.today();
                Date sevenDays = todaysDate.addDays(7);
                oo.CloseDate = sevenDays;
                oo.Kocsik__c = lineSplit[2];
                oo.Name      = lineSplit[3];
                oo.StageName = lineSplit[4];
                
                oppList.add(oo);
            }
            
        }

    	insert oppList;      
    	return result;
    }
    

}
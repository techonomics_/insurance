// Generated by Illuminated Cloud on Mon Jan 20 20:25:46 CET 2020. Do not edit.

global class OpportunityLineItem extends SObject 
{
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global String Description;
    global Decimal Discount;
    global Boolean IsDeleted;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Currency ListPrice;
    global String Name;
    global Opportunity Opportunity;
    global Id OpportunityId;
    global QuoteLineItem OpportunityLineItem;
    global PricebookEntry PricebookEntry;
    global Id PricebookEntryId;
    global Product2 Product2;
    global Id Product2Id;
    global String ProductCode;
    global Decimal Quantity;
    global FlowRecordRelation RelatedRecord;
    global Date ServiceDate;
    global SObjectType SObjectType;
    global Integer SortOrder;
    global Currency Subtotal;
    global Datetime SystemModstamp;
    global Currency TotalPrice;
    global Currency UnitPrice;

    global OpportunityLineItem()
    {
    }
}
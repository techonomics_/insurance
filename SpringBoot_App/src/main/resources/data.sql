insert into role (id, name) values (1, 'ROLE_ANONYMOUS');
insert into role (id, name) values (2, 'ROLE_USER');
insert into role (id, name) values (3, 'ROLE_ADMINKA');
insert into User (ID, PASSWORD, USERNAME) values (-1, '$2a$10$A31jE3qCkFePerFJl5YNJu2UNO4j0dWMW.RsOVtpyi374ukCutGzK', 'bandi@mail.com');
insert into User (ID, PASSWORD, USERNAME) values (-2, '$2a$10$A31jE3qCkFePerFJl5YNJu2UNO4j0dWMW.RsOVtpyi374ukCutGzK', 'kocsis@mail.com');
-- insert into User (ID, ACCOUNT_ID_SALES_FORCE, CONTACT_ID_SALES_FORCE, PASSWORD, USERNAME) values (-1, '', '', '$2a$10$A31jE3qCkFePerFJl5YNJu2UNO4j0dWMW.RsOVtpyi374ukCutGzK', 'bandi@mail.com');
insert into USER_ROLES (USERS_ID, ROLES_ID) values (-1, 3);
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Request a new Insurance policy</title>
        <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
        <link href="${contextPath}/resources/css/common.css" rel="stylesheet">


        <script>

            function getVals(inputs, inputs2, inputs3) {
                var str = '';
                let i = 0;
                Array.from(inputs).forEach(function (item) {

                    let car = item.value;
                    let year = inputs2[i].value;
                    let qqq = inputs3[i].value;
                    i++;

                    str += car + '/' + year + '/' + qqq;
                    if (i !== inputs.length) {
                        str += '|';
                    }

                });

                return str;
            }

        </script>

    </head>
    <body>
        <h1>Policy Request</h1>
        <h2>${info}</h2>

        <div class="container">


            <c:url var="actionUrl" value="/policy" />

            <form:form action="${actionUrl}" modelAttribute="policyForm" method="POST" acceptCharset="UTF-8" class="form-signin">
                <h2 class="form-signin-heading">Create your policy</h2>
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <form:input path="opportunityName"  placeholder="Adj nevet"/>
                    <form:errors path="opportunityName" />
                </div>

                <div class="form-group ${status.error ? 'has-error' : ''}">        
                    <form:input type="hidden" id="kocsik" path="vehicleString" />
                    <form:errors path="vehicleString" />
                </div>
                <button id="submitButton" style="visibility: hidden;" type="submit" value="Submit">Add Policy</button>

            </form:form>

            <!--            <input  class="inputCar"type="text"  name="a1" value="autoMarka"> </input>-->
            <select class="inputCar">
                <c:forEach items="${carListSelection}" var="item">
                    <option value="${item}">${item}</option>
                </c:forEach>
            </select>


            <input  class="inputYear"type="number"  name="y1" value="2010" min="1980" max="2020"> </input>       
            <input  class="inputQuantity"type="number"  name="q1" value="1" min="1" max="100"> </input><br><br>      
            <select class="inputCar">
                <c:forEach items="${carListSelection}" var="item">
                    <option value="${item}">${item}</option>
                </c:forEach>
            </select>
            <input  class="inputYear"type="number"  name="y2" value="2010" min="1980" max="2020"> </input>       
            <input  class="inputQuantity"type="number"  name="q2" value="1" min="1" max="100"> </input><br>   


            <button  id="decoySubmit" class="btn btn-lg btn-primary btn-block" >KLIKK IDE</button>


        </div>

        <script>

            let elem = document.getElementById('decoySubmit');
            elem.addEventListener("click", function () {
                let elemArrCar = document.getElementsByClassName('inputCar');
                let elemArrYear = document.getElementsByClassName('inputYear');
                let elemArrQ = document.getElementsByClassName('inputQuantity');
                console.log(getVals(elemArrCar, elemArrYear, elemArrQ));

                document.getElementById('kocsik').value = getVals(elemArrCar, elemArrYear, elemArrQ);
                document.getElementById('submitButton').click();
            });

        </script>





        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="${contextPath}/resources/js/bootstrap.min.js"></script>


    </body>
</html>







package bh10.security.repository;

import bh10.security.model.Policy;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
@Transactional
public interface PolicyRepository extends JpaRepository<Policy, Long>{

    @Query("Select p From Policy p Where p.ownerAccount=:pSalesForceAccountId")
    List<Policy> findByOwnerAccountId(@Param("pSalesForceAccountId") String pSalesForceAccountId);
    
}

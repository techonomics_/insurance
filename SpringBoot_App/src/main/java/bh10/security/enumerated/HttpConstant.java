package bh10.security.enumerated;


public class HttpConstant {


public static final long MAX_TIMEOUT_HTTPCLIENT_MILLISEC = 7000;
public static final long CRONJOB_INTERVAL_MILLISEC = 15000;
public static final int HTTP_RESPONSE200 = 200;
public static final int HTTP_RESPONSE300 = 300;
public static final int CONTACT_FILE_INITIAL_SIZE = 0;
}

package bh10.security.enumerated;


public enum PolicyStage {

NOT_YET_ADMITTED("Meg nincs benne"),
BEING_PROCESSED("Feldolgozas alatt"),
CANCELLED("lemondva"),
ADMITTED("kesz");

    
private final String message;


private PolicyStage(String message){
    this.message = message;
}

public String getMessage(){
    return this.message;
}

}

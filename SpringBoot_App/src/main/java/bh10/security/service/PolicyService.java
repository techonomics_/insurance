package bh10.security.service;

import bh10.security.dto.PolicyDTO;
import bh10.security.model.Policy;
import java.util.List;


public interface PolicyService {


    void save(Policy policy);
    
    List<PolicyDTO> findByOwnerAccountId(String pSalesForceAccountId);
    
}

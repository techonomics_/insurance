package bh10.security.service;

import bh10.security.dto.AccountDTO;
import bh10.security.mapper.AccountMapper;
import bh10.security.model.Account;
import bh10.security.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService implements IAccountService {

    @Autowired
    AccountRepository accountRepository;

    @Override
    public AccountDTO findBySalesForceId(String pSalesForceAccountId) {

        Account accEntity = accountRepository.findBySalesForceId(pSalesForceAccountId);
        return AccountMapper.mapAccountEntityToDto(accEntity);

    }

    @Override
    public Account save(Account account) {
        return accountRepository.save(account);
    }

}

package bh10.security.service;

import bh10.security.bean.Contact;
import bh10.security.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
    
    User updateSfContactId(Contact contact);
    
    String getContactIdForUser(String username);
    
}

package bh10.security.service;

import bh10.security.IAuthenticationFacade;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public class SecurityServiceImpl implements SecurityService{
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    
    @Autowired
    private IAuthenticationFacade authenticationFacade;
    
    private static final Logger logger = LoggerFactory.getLogger(SecurityServiceImpl.class);

    @Override
    public String findLoggedInUsername() {
      //  Object userDetails = SecurityContextHolder.getContext().getAuthentication().getDetails();        

         Authentication authentication = authenticationFacade.getAuthentication();
         System.out.println("BBAAAAEEEDUNNNGG: " + authentication.getName());
         
         return authentication.getName();      
        
        
        
        
        
//         Authentication userDetails2 = SecurityContextHolder.getContext().getAuthentication();
//        Collection<? extends GrantedAuthority>  auau = userDetails2.getAuthorities();
//        for (GrantedAuthority grantedAuthority : auau) {
//            System.out.println("EZ a JOGA: " + grantedAuthority.getAuthority());
//        }
//        
//        if (userDetails instanceof UserDetails) {
//            System.out.println("NA most a neve..." + ((UserDetails)userDetails).getUsername());
//            return ((UserDetails)userDetails).getUsername();
//        }
//
//        return null;
    }

    @Override
    public void autoLogin(String username, String password) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());
       
        
        authenticationManager.authenticate(usernamePasswordAuthenticationToken);        
        
        if (usernamePasswordAuthenticationToken.isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            logger.debug(String.format("Auto login %s successfully!", username));
        }
    }
}

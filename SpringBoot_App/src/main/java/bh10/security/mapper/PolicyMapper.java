package bh10.security.mapper;

import bh10.security.dto.PolicyDTO;
import bh10.security.model.Policy;
import java.util.ArrayList;
import java.util.List;


public class PolicyMapper {


    public static PolicyDTO mapPolicyEntityToDto(Policy policy){
        
        PolicyDTO policyDTO = new PolicyDTO();
        
        policyDTO.setCreatedByUser(policy.getCreatedByUser());
        policyDTO.setDateOfCreation(policy.getDateOfCreation());
        policyDTO.setId(policy.getId());
        policyDTO.setOpportunityName(policy.getOpportunityName());
        policyDTO.setOwnerAccount(policy.getOwnerAccount());
        policyDTO.setVehicleString(policy.getVehicleString());
        policyDTO.setPolicyStage(policy.getPolicyStage());
        
        return policyDTO;
    }
    
    
    public static List<PolicyDTO> mapPolicyEntityListToDto(List<Policy> entityList){
        
        List<PolicyDTO> dtoList = new ArrayList<>();
        
        entityList.forEach((policy) -> {
            dtoList.add(mapPolicyEntityToDto(policy));
        });
        
        return dtoList;
    }
    
}















package bh10.security.util;



import bh10.security.enumerated.HttpConstant;
import bh10.security.model.Policy;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class HttpClientPost {
    public static void main(String[] args) {
        
        Policy policy = new Policy.Builder()
                .setCreatedByUser("Bandi")
                .setDateOfCreation(LocalDate.now())
                .setOwnerAccount("BRutiiii")
                .setOppName("FatChance")
                .setvehicleString("Dacia/1989/1")
                .build();
        sendPolicyContent(policy);
        
    }
        
     public static void sendPolicyContent(Policy policy){   
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(
                HttpConstant.SALESFORCE_OPPORTUNITY_URL);

        try {

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
            nameValuePairs.add(new BasicNameValuePair("secret", "secret"));
            nameValuePairs.add(new BasicNameValuePair("accountId", policy.getOwnerAccount()));
            nameValuePairs.add(new BasicNameValuePair("opportunityName", policy.getOpportunityName()));
            nameValuePairs.add(new BasicNameValuePair("kocsi", policy.getVehicleString()));

            
            post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = client.execute(post);
            BufferedReader rd = new BufferedReader(new InputStreamReader(
                    response.getEntity().getContent()));

            String line = "";
            while ((line = rd.readLine()) != null) {
                System.out.println(line);
                if (line.startsWith("Auth=")) {
                    String key = line.substring(5);
                    // do something with the key
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

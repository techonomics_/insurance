package bh10.security.util;

import bh10.security.enumerated.HttpConstant;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class Scheduler {

    @Autowired
    HttpClient httpClient;
    
    @Autowired
    HttpClient2 httpClient2;

    @Autowired
    CarListCache carListCache;
    
    @Scheduled(fixedRate = HttpConstant.CRONJOB_INTERVAL_MILLISEC)
    public void fixedRateSch() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        System.out.println(strDate);
        httpClient2.getContent();
        httpClient.getContent();
        carListCache.downloadContent();
    }
}

package bh10.security.model;

import bh10.security.enumerated.PolicyStage;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Policy implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDate dateOfCreation;

    private String createdByUser;

    private String ownerAccount;

    private String opportunityName;

    private String vehicleString;

    private PolicyStage policyStage;
    
    
    public Policy() {
    }

    private Policy(Builder b) {
        this.id = b.id;
        this.createdByUser = b.createdByUser;
        this.dateOfCreation = b.dateOfCreation;
        this.ownerAccount = b.ownerAccount;
        this.vehicleString = b.vehicleString;
        this.opportunityName = b.opportunityName;
        this.policyStage = b.policyStage;
    }

    public static class Builder {

        private Long id;
        private LocalDate dateOfCreation;
        private String createdByUser;
        private String ownerAccount;
        private String vehicleString;
        private String opportunityName;
        private PolicyStage policyStage;

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public Builder setOwnerAccount(String ownerAccount) {
            this.ownerAccount = ownerAccount;
            return this;
        }

        public Builder setDateOfCreation(LocalDate dateOfCreation) {
            this.dateOfCreation = dateOfCreation;
            return this;
        }

        public Builder setCreatedByUser(String createdByUser) {
            this.createdByUser = createdByUser;
            return this;
        }

        public Builder setvehicleString(String vehicleString) {
            this.vehicleString = vehicleString;
            return this;
        }

        public Builder setOppName(String opportunityName) {
            this.opportunityName = opportunityName;
            return this;
        }

        public Builder setPolicyStage(PolicyStage policyStage){
            this.policyStage = policyStage;
            return this;
        }
        
        public Policy build() {
            return new Policy(this);
        }

    }

    
    
    public PolicyStage getPolicyStage() {
        return policyStage;
    }

    public void setPolicyStage(PolicyStage policyStage) {
        this.policyStage = policyStage;
    }

    public String getOpportunityName() {
        return opportunityName;
    }

    public void setOpportunityName(String opportunityName) {
        this.opportunityName = opportunityName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(LocalDate dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public String getCreatedByUser() {
        return createdByUser;
    }

    public void setCreatedByUser(String createdByUser) {
        this.createdByUser = createdByUser;
    }

    public String getOwnerAccount() {
        return ownerAccount;
    }

    public void setOwnerAccount(String ownerAccount) {
        this.ownerAccount = ownerAccount;
    }

    public String getVehicleString() {
        return vehicleString;
    }

    public void setVehicleString(String vehicleString) {
        this.vehicleString = vehicleString;
    }

}

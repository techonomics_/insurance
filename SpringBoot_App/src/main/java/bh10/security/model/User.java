package bh10.security.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;

    private String password;

    private String contactIdSalesForce;
    
    private String accountIdSalesForce;

    private String firstName;
    private String lastName;
    
    
    @Transient
    private String passwordConfirm;

    @ManyToMany
    private Set<Role> roles;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    
    
    
    
    public String getAccountIdSalesForce() {
        return accountIdSalesForce;
    }

    public void setAccountIdSalesForce(String accountIdSalesForce) {
        this.accountIdSalesForce = accountIdSalesForce;
    }

    
    public String getContactIdSalesForce() {
        return contactIdSalesForce;
    }

    public void setContactIdSalesForce(String contactIdSalesForce) {
        this.contactIdSalesForce = contactIdSalesForce;
    }

    
    
    
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}
